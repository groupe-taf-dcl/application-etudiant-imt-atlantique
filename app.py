import flask

from flask import Flask, render_template, request, redirect, url_for
from flask import jsonify , send_file, make_response
from io import BytesIO

import useful_func
from database.database import db, init_database
from database.models import Personne, Thematique, Pfe, Organisation, Position
import os

from forms_funcs import *
from useful_func import *
absolute_path = os.path.dirname(__file__)
relative_path = "database/database.db"
full_path = os.path.join(absolute_path, relative_path)

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://///" + full_path
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SECRET_KEY"] = "secret_key1234"
db.init_app(app)  # (1) flask prend en compte la base de donnee
with app.test_request_context():  # (2) bloc exécuté à l'initialisation de Flask
    init_database()

with app.app_context():
    #etudiants = get_all_etudiants()
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    promotions = get_all_promotions()



@app.route('/edit/<string:statut>/<int:id>/', methods=["GET", "POST"])
def display_profile(id, statut):
    form_est_valide_profil, errors = formulaire_est_valide_profil(flask.request.form)
    user = Personne.query.filter_by(id=id).first()

    if not form_est_valide_profil:
         return  afficher_formulaire_profil(flask.request.form, errors, user=user, statut=statut)
    else:
        return edit_formulaire_profil(flask.request.form, user=user, statut=statut)

@app.route('/addNewCompany', methods=["POST"])
def ajouter_entreprise():
    try :
        nom_entreprise=request.form['newCompanyName']
        description_entreprise=request.form['newCompanyDescription']

        # Enregistre les données dans la base de données
        new_company = Organisation(nom_organisation=nom_entreprise, description_organisation=description_entreprise)
        db.session.add(new_company)
        db.session.commit()
        return jsonify({'success':True,'id': new_company.id, 'nom': new_company.nom_organisation})

    except :
        return jsonify({'success': False})



@app.route('/addCompany', methods=["GET", "POST"])
def display_profile_with_new_company():
    form_est_valide_profil_new_company, errors = formulaire_est_valide_profil_new_company(flask.request.form)
    if not form_est_valide_profil_new_company:
        return afficher_formulaire_profil_new_company(flask.request.form, errors, form_est_valide_profil_new_company)
    else:
        return traitement_formulaire_profil_new_company(flask.request.form)

@app.route('/<string:statut>/<int:id>/', methods=["GET", "POST"])
def display_dashboard(id,statut):
    etudiants = get_all_etudiants()
    organisations=get_all_organisations()
    tafs=get_all_thematiques()
    promotions=get_all_promotions()
    nom_organisations = []
    nom_tafs=[]
    nom_promotions_string=[]
    nom_promotions=[]
    company=""

    if request.method == "POST":
        nom_organisations = request.form.getlist("select_organisation")
        nom_tafs = request.form.getlist("select_taf")
        nom_promotions_string = request.form.getlist("select_promotion")
        for nom_promotion_string in nom_promotions_string:
            nom_promotions.append(int(nom_promotion_string))

    user=Personne.query.filter_by(id=id).first()
    etudiants = UpdateDashboard(nom_organisations,nom_tafs,nom_promotions)
    position= Position.query.filter_by(id_personne=user.id).first()
    if (position):
        company = Organisation.query.filter_by(id=position.id_organisation).first()
    return flask.render_template('dashboard.html.jinja2', company=company, statut=statut, user=user, tafs=tafs, etudiants=etudiants, promotions = promotions, organisations=organisations, nom_organisations=nom_organisations, nom_tafs=nom_tafs, nom_promotions=nom_promotions)
@app.route('/displayprofile/<string:statut>/<int:id>', methods=["GET","POST"])
def display_viewProfile(id, statut):

    etudiant = Personne.query.filter_by(id=id).first()
    position = Position.query.filter_by(id_personne = etudiant.id).first()
    company=""
    if (position):
        company = Organisation.query.filter_by(id=position.id_organisation).first()
    return flask.render_template('viewProfile.html.jinja2', user=etudiant, currentposition=position, company=company, statut=statut)

@app.route('/', methods=['GET','POST'])
def display_logIn():
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    id=0
    company=""
    if request.method == "POST":
        statut = request.form.get('statutUser')
        if statut == "user":
            id = request.form["user_id"]
            user_id = int(id)
            user = Personne.query.filter_by(id=id).first()
            position = Position.query.filter_by(id_personne=user.id).first()
            if (position):
                company=Organisation.query.filter_by(id=position.id_organisation).first()
            return flask.render_template('viewProfile.html.jinja2', user=user, company=company, currentposition=position, statut=statut, user_id=user_id )
        elif statut == "admin":
            tafs = get_all_thematiques()
            organisations = get_all_organisations()
            etudiants = get_all_etudiants()
            promotions = get_all_promotions()
            return flask.render_template('tableTemplate/etudiantTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)
    return flask.render_template('login.html.jinja2')

@app.route('/edit/<string:statut>', methods=['GET','POST'])
def display_admin(statut):
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    categorie='etudiants'

    if request.method == "POST":
        categorie=request.form['selectCategorie']
    if(categorie=='etudiants'):
        return flask.render_template('tableTemplate/etudiantTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)
    elif (categorie == 'tafs'):
        return flask.render_template('tableTemplate/tafTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations, promotions=promotions)
    elif (categorie == 'entreprises'):
        return flask.render_template('tableTemplate/entrepriseTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations, promotions=promotions)
    elif (categorie == 'promotions'):
        return flask.render_template('tableTemplate/promotionTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations, promotions=promotions)

@app.route('/delete_student/<string:statut>', methods=['GET','POST'])
def delete_student(statut):
    etudiant_id = flask.request.form['student']
    delete_Personne_By_Id(etudiant_id)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/etudiantTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)

@app.route('/delete_promotion/<string:statut>', methods=['GET','POST'])
def delete_promotion(statut):
    promotion_id = flask.request.form['promotion_id']
    delete_Promotion_By_Id(promotion_id)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/promotionTable.html.jinja2', statut=statut,etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)

@app.route('/create_promotion/<string:statut>', methods=['GET','POST'])
def create_promotion(statut):
    name_promotion=flask.request.form['name_promotion']
    year_promotion=flask.request.form['year_promotion']
    create_Promotion(name_promotion,year_promotion)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/promotionTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs,
                                 organisations=organisations, promotions=promotions)

@app.route('/edit_promotion/<string:statut>', methods=['GET','POST'])
def edit_promotion(statut):
    promotion_id = flask.request.form['promotion_id']
    promotion = Promotion.query.filter_by(id = promotion_id).first()
    name_promotion=flask.request.form['name_promotion']
    year_promotion=flask.request.form['year_promotion']
    modify_Promotion(promotion, name_promotion,year_promotion)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/promotionTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs,
                                 organisations=organisations, promotions=promotions)

@app.route('/delete_taf/<string:statut>', methods=['GET','POST'])
def delete_taf(statut):
    taf_id = flask.request.form['taf_id']
    print(taf_id)
    delete_Thematique_By_Id(taf_id)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/tafTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)

@app.route('/create_taf/<string:statut>', methods=['GET','POST'])
def create_taf(statut):
    name_taf = flask.request.form['name_taf']
    date_taf = flask.request.form['date_taf']
    create_Taf(name_taf,date_taf)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/tafTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)

@app.route('/edit_taf/<string:statut>', methods=['GET','POST'])
def edit_taf(statut):
    taf_id = flask.request.form['taf_id']
    taf = Thematique.query.filter_by(id = taf_id).first()
    name_taf = flask.request.form['name_taf']
    date_taf = flask.request.form['date_taf']
    modify_Taf(taf,name_taf,date_taf)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/tafTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)

@app.route('/delete_company/<string:statut>', methods=['GET','POST'])
def delete_company(statut):
    company_id = flask.request.form['company_id']
    delete_Organisation_By_Id(company_id)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/entrepriseTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)


@app.route('/create_company/<string:statut>', methods=['GET','POST'])
def create_company(statut):
    name_company = flask.request.form['name_company']
    description_company = flask.request.form['description_company']
    create_Organisation(name_company,description_company)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/entrepriseTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)


@app.route('/edit_company/<string:statut>', methods=['GET','POST'])
def edit_company(statut):
    company_id = flask.request.form["company_id"]
    company = Organisation.query.filter_by(id=company_id).first()
    name_company = flask.request.form['name_company']
    description_company = flask.request.form['description_company']
    modify_Organisation(company,name_company,description_company)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/entrepriseTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)

@app.route('/create_student/<string:statut>', methods=['GET','POST'])
def create_student(statut):
    etudiantSurname = flask.request.form['etudiantSurname']
    etudiantName = flask.request.form['etudiantName']
    email = flask.request.form['email']
    name_taf_2eme_annee = flask.request.form['taf_2eme_annee']
    name_taf_3eme_annee = flask.request.form['taf_3eme_annee']
    taf_2eme_annee = Thematique.query.filter_by(nom_taf=name_taf_2eme_annee).first()
    print(taf_2eme_annee)
    taf_3eme_annee = Thematique.query.filter_by(nom_taf=name_taf_3eme_annee).first()
    print(taf_3eme_annee)
    name_promotion = flask.request.form['name_promotion']
    promotion = Promotion.query.filter_by(year=name_promotion).first()
    new_Student = create_Personne(etudiantSurname, etudiantName, email, [taf_2eme_annee, taf_3eme_annee], promotion)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/etudiantTable.html.jinja2',statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)

@app.route('/edit_student/<string:statut>', methods=['GET','POST'])
def edit_student(statut):
    etudiantSurname = flask.request.form['etudiantSurname']
    etudiantName = flask.request.form['etudiantName']
    email = flask.request.form['email']
    name_taf_2eme_annee = flask.request.form['taf_2eme_annee']
    name_taf_3eme_annee = flask.request.form['taf_3eme_annee']
    taf_2eme_annee = Thematique.query.filter_by(nom_taf=name_taf_2eme_annee).first()
    taf_3eme_annee = Thematique.query.filter_by(nom_taf=name_taf_3eme_annee).first()
    name_promotion = flask.request.form['name_promotion']
    promotion = Promotion.query.filter_by(year=name_promotion).first()
    personne_id = flask.request.form['personne_id']
    personne = Personne.query.filter_by(id = personne_id).first()
    new_personne = modify_Personne(personne, etudiantSurname, etudiantName, email, [taf_2eme_annee, taf_3eme_annee], promotion)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    etudiants = get_all_etudiants()
    promotions = get_all_promotions()
    return flask.render_template('tableTemplate/etudiantTable.html.jinja2', statut=statut, etudiants=etudiants, tafs=tafs, organisations=organisations,promotions=promotions)

@app.route('/rechercher', methods=['POST'])
def rechercher():
    input_value = request.form['input_value']
    categorie = request.form['categorie']

    if input_value == None:
        input_value = ""

    if categorie == "etudiants":
        try:
            etudiants = Personne.query.filter(Personne.nom.like('%' + input_value + '%') | Personne.prenom.like('%' + input_value + '%')).all()
            serialized_etudiants = [etudiant.serialize() for etudiant in etudiants]

            return jsonify({'success': True, 'etudiants': serialized_etudiants})
        except Exception as e:
            print(e)
            return jsonify({'success': False})

    if categorie == "promotions":
        try:
            promotions = Promotion.query.filter(Promotion.name_promotion.like('%' + input_value + '%') | Promotion.year.like('%' + input_value + '%')).all()
            serialized_promotions = [promotion.serialize() for promotion in promotions]
            return jsonify({'success': True, 'promotions': serialized_promotions})
        except Exception as e:
            print(e)
            return jsonify({'success': False})

    if categorie == "entreprises":
        try:
            entreprises = Organisation.query.filter(Organisation.nom_organisation.like('%' + input_value + '%') | Organisation.description_organisation.like('%' + input_value + '%')).all()
            serialized_entreprises = [entreprise.serialize() for entreprise in entreprises]
            return jsonify({'success': True, 'entreprises': serialized_entreprises})
        except Exception as e:
            print(e)
            return jsonify({'success': False})
    if categorie == "tafs":
        try:
            tafs = Thematique.query.filter(Thematique.nom_taf.like('%' + input_value + '%') | Thematique.date.like('%' + input_value + '%')).all()
            serialized_tafs = [taf.serialize() for taf in tafs]
            return jsonify({'success': True, 'tafs': serialized_tafs})
        except Exception as e:
            print(e)
            return jsonify({'success': False})



@app.route('/pdf/<int:pfe_id>')
def view_pdf(pfe_id):
    pfe = Pfe.query.get(pfe_id)
    pdf = BytesIO()
    pdf.write(pfe.report_pfe)
    pdf.seek(0)
    response = make_response(send_file(pdf, mimetype='report_pfe/pdf', download_name='report_pfe.pdf'))
    response.headers['Content-Type'] = 'report_pfe/pdf'
    return response




''' 
@app.route('/signup', methods=['POST'])
def display_signup():
    email = request.form.get('email')
    name = request.form.get('name')
    surname = request.form.get('surname')
    password = request.form.get('password')

    user = Personne.query.filter_by(email=email).first()  # if this returns a user, then the email already exists in database
    if user:  # if a user is found, we want to redirect back to signup page so user can try again
        flash('Adresse email déjà existante')
        return redirect(url_for('signup.html.jinja2'))
    # create a new user with the form data. Hash the password so the plaintext version isn't saved.
    new_user = Personne(email=email, name=name,nom=surname, password=generate_password_hash(password, method='sha256'))

    # add the new user to the database
    db.session.add(new_user)
    db.session.commit()
    return flask.render_template('login.html.jinja2')
'''



if __name__ == '__main__':
    app.run()
