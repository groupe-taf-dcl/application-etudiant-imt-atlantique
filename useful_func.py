from database.models import *
from database.database import db, init_database


def delete_db_object(db_object):
    db.session.delete(db_object)
    db.session.commit()
    return None

def add_db_object(db_object):
    db.session.add(db_object)
    db.session.commit()
    return None
def create_Personne(nom, prenom, email, tafs, promotion):
    new_personne = Personne(nom=nom, prenom=prenom, email=email, tafs=tafs, promotion=promotion)
    add_db_object(new_personne)
    return new_personne

def modify_Personne(personne, new_nom, new_prenom, new_email, new_Tafs, new_promotion):
    personne.nom = new_nom
    personne.prenom = new_prenom
    personne.email = new_email
    personne.tafs = new_Tafs
    personne.promotion = new_promotion
    add_db_object(personne)
    return personne
def create_Position(id_personne, id_organisation, position, date_entre):
    new_position = Position(id_personne=id_personne, id_organisation=id_organisation, position=position, date_entre=date_entre)
    add_db_object(new_position)
    return new_position

def modify_Position(position, id_personne, id_organisation, position_description, date_entre):
    position.id_personne=id_personne
    position.id_organisation=id_organisation
    position.date_entre=date_entre
    position.position=position_description
    add_db_object(position)
def create_Pfe(nom_tuteur, email_tuteur, summary, titre, organisation, personne, report_pfe):
    new_pfe = Pfe(nom_tuteur=nom_tuteur, email_tuteur=email_tuteur, summary=summary, titre=titre, organisation=organisation, owner=personne, report_pfe=report_pfe)
    add_db_object(new_pfe)
    return(new_pfe)

def modify_titrePfe(pfe, titre):
    pfe.titre=titre
    add_db_object(pfe)

def modify_tuteurPfe(pfe, nom_tuteur):
    pfe.nom_tuteur=nom_tuteur
    add_db_object(pfe)

def modify_emailPfe(pfe, email_tuteur):
    pfe.email_tuteur=email_tuteur
    add_db_object(pfe)
def modify_summaryPfe(pfe, summary):
    pfe.summary=summary
    add_db_object(pfe)

def modify_companyPfe(pfe, company):
    pfe.company_id=company.id
    add_db_object(pfe)

def modify_pdfPfe(pfe, report):
    pfe.report_pfe=report
    add_db_object(pfe)
def create_Taf(nom_taf, date):
    new_taf = Thematique(nom_taf=nom_taf, date=date)
    add_db_object(new_taf)
    return None
def delete_Taf(taf):
    delete_db_object(taf)
    return None
def modify_Taf(taf, new_nom_taf, new_date):
    taf.nom_taf = new_nom_taf
    taf.date = new_date
    add_db_object(taf)
    return None
def create_Organisation(nom_organisation, description_organisation):
    new_Organisation = Organisation(nom_organisation=nom_organisation,description_organisation=description_organisation)
    add_db_object(new_Organisation)
    return None
def delete_Organisation(organisation):
    delete_db_object(organisation)
    return None
def modify_Organisation(organisation, new_nom_organisation, new_description_organisation):
    organisation.nom_organisation = new_nom_organisation
    organisation.description_organisation = new_description_organisation
    add_db_object(organisation)
    return None

def create_Promotion(name_promotion, year):
    new_Promotion = Promotion(name_promotion=name_promotion,year=year)
    add_db_object(new_Promotion)
    return None
def delete_Promotion(promotion):
    delete_db_object(promotion)
    return None
def modify_Promotion(promotion, new_name_promotion, new_year):
    promotion.name_promotion = new_name_promotion
    promotion.year = new_year
    add_db_object(promotion)
    return None


def get_all_promotions():
    All_promotions = Promotion.query.all()
    return All_promotions
def get_all_thematiques():
    All_tafs = Thematique.query.all()
    return All_tafs
def get_all_organisations():
    All_organisations = Organisation.query.all()
    return All_organisations

def get_all_etudiants():
    All_etudiants = Personne.query.all()
    return All_etudiants

def delete_Personne_By_Id(id):
    personne = Personne.query.filter_by(id=id).first()
    delete_db_object(personne)
    return None
def delete_Promotion_By_Id(id):
    promotion = Promotion.query.filter_by(id=id).first()
    delete_db_object(promotion)
    return None
def delete_Thematique_By_Id(id):
    thematique = Thematique.query.filter_by(id=id).first()
    delete_db_object(thematique)
    return None
def delete_Organisation_By_Id(id):
    organisation = Organisation.query.filter_by(id=id).first()
    delete_db_object(organisation)
    return None


def UpdateDashboard(nom_organisations,nom_tafs, nom_promotions):
    etudiants_query = Personne.query
    if nom_promotions:
        id_select_promotions = []
        for nom_promotion in nom_promotions:
            promotion = Promotion.query.filter_by(year=nom_promotion).first()
            id_select_promotions.append(promotion.id)
        etudiants_query = etudiants_query.filter(Personne.promotion_id.in_(id_select_promotions))
    if nom_organisations:
        id_select_organisations = []
        for nom_organisation in nom_organisations:
            organisation = Organisation.query.filter_by(nom_organisation=nom_organisation).first()
            id_select_organisations.append(organisation.id)
        etudiants_query = etudiants_query.filter(Position.id_organisation.in_(id_select_organisations), Position.id_personne == Personne.id)
    if nom_tafs:
        id_select_tafs = []
        for nom_taf in nom_tafs:
            taf = Thematique.query.filter_by(nom_taf=nom_taf).first()
            id_select_tafs.append(taf.id)
        etudiants_query = etudiants_query.join(parcours).join(Thematique).filter(Thematique.id.in_(id_select_tafs))

    etudiants = etudiants_query.all()
    return etudiants