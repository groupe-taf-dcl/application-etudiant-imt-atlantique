##Application Alumni IMTAtlantique

Ce référentiel contient du code pour la communication entre la base de données et le frontend d'une application web. Les informations suivantes expliquent la structure du code :

#useful_func.py

Ce fichier contient toutes les fonctions qui sont utiles pour la communication entre la base de données et le frontend. Ces fonctions comprennent l'ajout, la suppression et la modification d'entités dans une table donnée.

#forms_func.py

Ce fichier contient toutes les fonctions liées aux formulaires dans l'application web. Ces fonctions comprennent le traitement des formulaires, l'affichage des formulaires et la validation des formulaires.

#templates

Tous les modèles Jinja utilisés dans l'application web sont stockés dans ce dossier. Les modèles liés aux tables affichées sur la page des paramètres d'administration sont stockés dans le sous-dossier tableTemplate.

Nous espérons que ces informations vous aideront à comprendre la structure de notre référentiel de code. Si vous avez des questions ou des préoccupations, n'hésitez pas à nous contacter.

#models.py

Ce fichier contient toutes les classes representants les differentes entités de notre base de données avec leur attribut.
