from database.database import db


parcours = db.Table('parcours',
                    db.Column('personne_id', db.Integer, db.ForeignKey('personne.id')),
                    db.Column('theamtique_id', db.Integer, db.ForeignKey('thematique.id'))
                    )
class Position(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_personne = db.Column(db.Integer, db.ForeignKey('personne.id'))
    id_organisation = db.Column(db.Integer, db.ForeignKey('organisation.id'))
    position = db.Column(db.Text)
    date_entre = db.Column(db.Integer)
class Thematique(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom_taf = db.Column(db.Text)
    date = db.Column(db.Integer)

    def serialize(self):
        return {
            'id': self.id,
            'nom_taf': self.nom_taf,
            'date': self.date,
        }
class Personne(db.Model):
    tafs = db.relation('Thematique', backref='eleves', secondary='parcours')
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.Text)
    email = db.Column(db.Text)
    prenom = db.Column(db.Text)
    pfe = db.relationship('Pfe', uselist=False, backref='owner')
    promotion_id = db.Column(db.Integer, db.ForeignKey('promotion.id'))
    organisations = db.relationship('Organisation', backref='personnes', secondary='position')
    def serialize(self):
            return {
                'id': self.id,
                'nom': self.nom,
                'prenom': self.prenom,
                'email': self.email,
                'promotion':self.promotion.serialize(),
                'tafs':[self.tafs[0].serialize(), self.tafs[1].serialize()]
            }
class Organisation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom_organisation = db.Column(db.Text)
    description_organisation = db.Column(db.Text)
    pfes = db.relationship('Pfe', backref='organisation')
    def serialize(self):
            return {
                'id': self.id,
                'nom_organisation': self.nom_organisation,
                'description_organisation': self.description_organisation,
            }

class Pfe(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom_tuteur = db.Column(db.Text)
    email_tuteur = db.Column(db.Text)
    summary = db.Column(db.Text)
    titre = db.Column(db.Text)
    report_pfe = db.Column(db.BLOB)
    company_id = db.Column(db.Integer, db.ForeignKey('organisation.id'))
    owner_id = db.Column(db.Integer, db.ForeignKey('personne.id'))


class Promotion(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name_promotion = db.Column(db.Text)
    year = db.Column(db.Integer)
    etudiants = db.relationship('Personne', backref='promotion')
    def serialize(self):
            return {
                'id': self.id,
                'name_promotion': self.name_promotion,
                'year': self.year,
            }

