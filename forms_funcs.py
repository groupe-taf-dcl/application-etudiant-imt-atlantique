import flask
from flask import Flask, render_template, request, redirect, url_for

from database.models import *
from useful_func import *
from app import *


def formulaire_est_valide_profil_new_company(form):
    newCompanyDescription = request.form.get("newCompanyDescription")
    newCompanyName = request.form.get("newCompanyName")
    result = True
    errors = []

    if newCompanyDescription == None or newCompanyDescription == "":
        result = False
        errors += ["missing 'newCompanyDescription' parameter"]
    if newCompanyName == None or newCompanyName == "":
        result = False
        errors += ["missing 'newCompanyName' parameter"]

    return result, errors


def afficher_formulaire_profil_new_company(form, errors,user,result):
    promotions = get_all_promotions()
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    print(errors, "Company not created")
    return flask.render_template('profil.html.jinja2', user=user,tafs=tafs, organisations=organisations, errors_new_company=result, promotions = promotions)


def traitement_formulaire_profil_new_company(form, user):
    newCompanyDescription = request.form.get("newCompanyDescription")
    newCompanyName = request.form.get("newCompanyName")
    create_Organisation(newCompanyName, newCompanyDescription)
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    promotions = get_all_promotions()
    return flask.render_template('profil.html.jinja2', user=user, tafs=tafs, organisations=organisations, promotions=promotions)


def formulaire_est_valide_profil(form):
    prenom = request.form.get("fname")
    nom = request.form.get("surname")
    email = request.form.get("email")
    nom_organisation_position = request.form.get("company_position")
    nom_organisation_pfe = request.form.get("company_pfe")
    taf_2eme_annee = request.form.get("taf_2eme_annee")
    taf_3eme_annee = request.form.get("taf_3eme_annee")
    result = True
    errors = []

    if taf_2eme_annee == taf_3eme_annee:
        result = False
        errors += ["can't choose two times the same taf"]

    if nom == None or nom == "":
        result = False
        errors += ["missing 'nom' parameter"]
    if prenom == None or prenom == "":
        result = False
        errors += ["missing 'prenom' parameter"]

    if email == "" or email == None:
        result = False
        errors += ["missing 'email' parameter"]
    if nom_organisation_position == None or nom_organisation_position == "":
        result = False
        errors += ["missing 'nom_organisation_position' parameter"]
    if nom_organisation_pfe == None or nom_organisation_pfe == "":
        result = False
        errors += ["missing 'nom_organisation_pfe' parameter"]


    return result, errors
def traitement_formulaire_profil(form, user):
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    promotions = get_all_promotions()
    nom = request.form.get("fname")
    prenom = request.form.get("surname")
    email = request.form.get("email")
    year_promotion = request.form.get("promotion")
    promotion = Promotion.query.filter_by(year = year_promotion).first()
    nom_taf_2eme_annee = request.form.get("taf_2eme_annee")
    taf_2eme_annee = Thematique.query.filter_by(nom_taf=nom_taf_2eme_annee).first()
    nom_taf_3eme_annee = request.form.get("taf_3eme_annee")
    taf_3eme_annee = Thematique.query.filter_by(nom_taf=nom_taf_3eme_annee).first()
    etudiant_cree = create_Personne(nom, prenom, email, [taf_2eme_annee, taf_3eme_annee], promotion)
    etudiants = get_all_etudiants()

    if request.form.get("company_position") and request.form.get("position_description"):
        nom_organisation_position = request.form.get("company_position")
        organisation_position = Organisation.query.filter_by(nom_organisation=nom_organisation_position).first()
        position_description = request.form.get("position_description")
        position_cree = create_Position(etudiant_cree.id, organisation_position.id, position_description, "2023")
        etudiants = get_all_etudiants()

    if (request.form.get("titre") and request.form.get("nom_tuteur") and request.form.get("email_tuteur") and request.form.get("company_pfe")):
        titre = request.form.get("titre")
        nom_tuteur = request.form.get("nom_tuteur")
        email_tuteur = request.form.get("email_tuteur")
        summary = request.form.get("summary")
        nom_organisation_pfe = request.form.get("company_pfe")
        organisation_pfe = Organisation.query.filter_by(nom_organisation=nom_organisation_pfe).first()
        report_pfe_file = request.files["internship_report"]
        print(report_pfe_file)
        report_pfe = report_pfe_file.read()
        pfe_cree = create_Pfe(nom_tuteur, email_tuteur, summary, titre, organisation_pfe, etudiant_cree,report_pfe)

    return flask.render_template('viewProfile.html.jinja2', etudiant=user ,tafs=tafs, organisations=organisations, promotions = promotions)

def edit_formulaire_profil(form, user, statut):
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    promotions = get_all_promotions()
    nom = request.form.get("fname")
    prenom = request.form.get("surname")
    email = request.form.get("email")
    year_promotion = request.form.get("promotion")
    promotion = Promotion.query.filter_by(year = year_promotion).first()
    nom_taf_2eme_annee = request.form.get("taf_2eme_annee")
    taf_2eme_annee = Thematique.query.filter_by(nom_taf=nom_taf_2eme_annee).first()
    nom_taf_3eme_annee = request.form.get("taf_3eme_annee")
    taf_3eme_annee = Thematique.query.filter_by(nom_taf=nom_taf_3eme_annee).first()
    etudiant_modif = modify_Personne(user,nom, prenom, email, [taf_2eme_annee, taf_3eme_annee], promotion)
    etudiants = get_all_etudiants()

    user_position=Position.query.filter_by(id_personne=user.id).first()
    user_company=""
    if request.form.get("company_position") and request.form.get("position_description") :
            nom_organisation_position = request.form.get("company_position")
            organisation_position = Organisation.query.filter_by(nom_organisation=nom_organisation_position).first()
            position_description = request.form.get("position_description")
            if (user_position):
                user_company = Organisation.query.filter_by(id=user_position.id_organisation).first()
                position_modifiee = modify_Position(user_position, etudiant_modif.id, organisation_position.id,position_description, "2023")
            else :
                position_cree = create_Position(etudiant_modif.id, organisation_position.id, position_description,
                                                "2023")
            etudiants = get_all_etudiants()

    if (user.pfe):
        if request.form.get("titre"):
            titre = request.form.get("titre")
            modify_titrePfe(user.pfe, titre)
        elif request.form.get("nom_tuteur"):
            nom_tuteur = request.form.get("nom_tuteur")
            modify_tuteurPfe(user.pfe, nom_tuteur)
        elif request.form.get("email_tuteur"):
            email_tuteur = request.form.get("email_tuteur")
            modify_emailPfe(user.pfe, email_tuteur)
        elif request.form.get("summary"):
            summary = request.form.get("summary")
            modify_summaryPfe(user.pfe,summary)
        elif request.form.get("company_pfe"):
            nom_organisation_pfe = request.form.get("company_pfe")
            organisation_pfe = Organisation.query.filter_by(nom_organisation=nom_organisation_pfe).first()
            modify_companyPfe(user.pfe, organisation_pfe)
        elif request.files["internship_report"]:
            report_pfe_file = request.files["internship_report"]
            report_pfe = report_pfe_file.read()
            modify_pdfPfe(user.pfe,report_pfe)

    elif (request.form.get("titre") and request.form.get("nom_tuteur") and request.form.get("email_tuteur") and request.form.get("company_pfe")):
        titre = request.form.get("titre")
        nom_tuteur = request.form.get("nom_tuteur")
        email_tuteur = request.form.get("email_tuteur")
        summary = request.form.get("summary")
        nom_organisation_pfe = request.form.get("company_pfe")
        organisation_pfe = Organisation.query.filter_by(nom_organisation=nom_organisation_pfe).first()
        report_pfe_file = request.files["internship_report"]
        print(report_pfe_file)
        report_pfe = report_pfe_file.read()
        pfe_cree = create_Pfe(nom_tuteur, email_tuteur, summary, titre, organisation_pfe, etudiant_modif,report_pfe)
    user_position = Position.query.filter_by(id_personne=user.id).first()
    if(user_position):
        user_company=Organisation.query.filter_by(id=user_position.id_organisation).first()
    return flask.render_template('viewProfile.html.jinja2', user=etudiant_modif, statut=statut, currentposition=user_position, company=user_company,tafs=tafs, organisations=organisations, promotions = promotions)

def afficher_formulaire_profil(form,errors, user, statut):
    tafs = get_all_thematiques()
    organisations = get_all_organisations()
    promotions = get_all_promotions()
    print(errors, "Profil not created")
    organisationactuelle=""
    user_position = Position.query.filter_by(id_personne=user.id).first()
    if(user_position):
        organisationactuelle=Organisation.query.filter_by(id=user_position.id_organisation).first()

    return flask.render_template('profil.html.jinja2', user=user, statut=statut, userposition=user_position,organisationactuelle=organisationactuelle, tafs=tafs, organisations=organisations, promotions=promotions, errors=errors)
